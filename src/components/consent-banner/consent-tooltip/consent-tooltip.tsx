import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'consent-tooltip',
  styleUrl: 'consent-tooltip.css',
  shadow: true,
})
export class ConsentTooltip {

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}

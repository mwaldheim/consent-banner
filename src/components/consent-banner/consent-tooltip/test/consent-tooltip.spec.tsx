import { newSpecPage } from '@stencil/core/testing';
import { ConsentTooltip } from '../consent-tooltip';

describe('consent-tooltip', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentTooltip],
      html: `<consent-tooltip></consent-tooltip>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-tooltip>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-tooltip>
    `);
  });
});

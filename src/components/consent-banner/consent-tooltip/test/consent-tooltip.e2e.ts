import { newE2EPage } from '@stencil/core/testing';

describe('consent-tooltip', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-tooltip></consent-tooltip>');

    const element = await page.find('consent-tooltip');
    expect(element).toHaveClass('hydrated');
  });
});

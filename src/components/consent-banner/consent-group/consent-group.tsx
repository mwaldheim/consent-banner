import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'consent-group',
  styleUrl: 'consent-group.css',
  shadow: true,
})
export class ConsentGroup {

  render() {
    return (
      <Host>
        <div id='NEEDED' class='swb_contens_entry_wrapper'>
          <div class='swb_contens_entry_title'>Notwendig (Können nicht abgeschaltet werden)</div>
          <consent-item></consent-item>
        </div>
      </Host>
    );
  }

}

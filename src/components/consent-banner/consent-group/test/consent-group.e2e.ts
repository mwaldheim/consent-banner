import { newE2EPage } from '@stencil/core/testing';

describe('consent-group', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-group></consent-group>');

    const element = await page.find('consent-group');
    expect(element).toHaveClass('hydrated');
  });
});

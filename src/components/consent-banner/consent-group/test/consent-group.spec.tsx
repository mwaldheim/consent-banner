import { newSpecPage } from '@stencil/core/testing';
import { ConsentGroup } from '../consent-group';

describe('consent-group', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentGroup],
      html: `<consent-group></consent-group>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-group>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-group>
    `);
  });
});

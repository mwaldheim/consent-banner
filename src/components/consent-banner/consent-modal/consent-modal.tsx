import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'consent-modal',
  styleUrl: 'consent-modal.css',
  shadow: true,
})
export class ConsentModal {

  render() {
    return (
      <Host>
        <div class='swb_contens loaded' role='dialog'>
          <div class='swb_contens_title'>🍪 Cookies und Datenschutzrichtlinien der Waldheim IT-Beratung</div>
          <div class='swb_contens_description'>Wir würden uns freuen, wenn Sie uns die Nutzung der Cookies erlauben
            würden, damit wir unsere Homepage noch besser an die Kundenwünsche anpassen können. Diese Webseite nutzt
            Cookies für Funktions-, Statistik-, Marketing- und Targetingzwecke. Durch das Klicken auf "<b>Alle
              akzeptieren &amp; speichern</b>" aktivieren Sie alle Cookies. Durch Klick auf "<b>Ausgewählte
              speichern</b>" aktivieren Sie nur die notwenigen bzw. die von Ihnen akzeptierten Cookies. Weitere
            Informationen finden Sie in unseren Datenschutzhinweisen.
          </div>
          <div class='swb_contens_details_title'>Cookie-Einstellungen</div>
          <div class='swb_contens_details_description'>Bestimmen Sie selbst, mit welchen Cookie-Einstellungen Sie
            einverstanden sind. Sie können diese Einstellung jederzeit wieder ändern.
          </div>
          // Groups
          <consent-group></consent-group>
          <div class='swb_contens_buttons'>
            <button class='secoundary outline'>Ausgewählte speichern</button>
            <button class='success'>Alle akzeptieren &amp; speichern</button>
          </div>
          <div class='swb_contens_links'><a
                                            href='/datenschutz/'>Datenschutz­bestimmungen</a><a
             href='/impressum/'>Impressum</a></div>
        </div>
      </Host>
    );
  }

}

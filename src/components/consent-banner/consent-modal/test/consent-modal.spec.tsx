import { newSpecPage } from '@stencil/core/testing';
import { ConsentModal } from '../consent-modal';

describe('consent-modal', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentModal],
      html: `<consent-modal></consent-modal>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-modal>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-modal>
    `);
  });
});

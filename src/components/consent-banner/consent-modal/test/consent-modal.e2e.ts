import { newE2EPage } from '@stencil/core/testing';

describe('consent-modal', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-modal></consent-modal>');

    const element = await page.find('consent-modal');
    expect(element).toHaveClass('hydrated');
  });
});

import { newSpecPage } from '@stencil/core/testing';
import { ConsentItem } from '../consent-item';

describe('consent-item', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentItem],
      html: `<consent-item></consent-item>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-item>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-item>
    `);
  });
});

import { newE2EPage } from '@stencil/core/testing';

describe('consent-item', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-item></consent-item>');

    const element = await page.find('consent-item');
    expect(element).toHaveClass('hydrated');
  });
});

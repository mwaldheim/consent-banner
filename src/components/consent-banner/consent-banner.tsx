import { Component, Host, h } from '@stencil/core';
import { cookie, ContensCookie, ContensJSON } from './class/cookie';


@Component({
  tag: 'consent-banner',
  styleUrl: 'consent-banner.css',
  shadow: true,
})
export class ConsentBanner {
  // JSON-Loadings
  json_buttons: any = {};
  json_cookies: any = {};
  json_details: any = {};
  json_dialog: any = {};
  json_links: any = {};
  json_types: any = {};
  json_types_aktiv: any = {};


  config = {
    delay: 0,
    debug: false,
    path: '/plugin/cookies/config/',
    extension: 'json',
  };

  cookies = new cookie(false);
  settings: {} = {};


  /**
   * Function try to load Element from Cookie or from Config
   * @param overload
   * @private
   */
  private async loadSettings(overload: boolean = false) {
    let json: any = {};
    if (overload) {
      const cookieName: ContensCookie = this.json_cookies.NEEDED['COOKIE_CONFIRM'];
      json = JSON.parse(this.cookies.get_cookie(cookieName.COOKIE_NAME));
    } else {
      for (const [key, value] of Object.entries(this.json_cookies)) {
        const wrapper = key;
        // key = wrapper
        for (const key of Object.entries(value)) {
          let value = 0;
          if (wrapper === 'NEEDED') {
            value = 1;
          }
          json[key[0]] = value;
        }
      }
    }
    this.settings = json;
  }

  /**
  * Function save the Cookie-Settings like it clickt and reload the Page, if not blocked
  * @param blockReload
  * @private
  */
  private save(blockReload: boolean = false) {
    const Elements = document.querySelectorAll('[data-cookiecontens="accepted"]');
    Elements.forEach((ele: HTMLElement) => {
      const cookiename = ele.dataset.cookiename;
      // @ts-ignore
      if (this.settings[cookiename] !== 1) {
        this.runScript(ele);
      }
    });
    this.cookies.set_cookie('COOKIE_CONFIRM_V3', JSON.stringify(this.settings), 365);
    if (!blockReload) {
      window.location.reload();
    }
  }



  /**
  * Function load File and split it into the Elements
  * @private
  */
  private async loadConfig(): Promise<boolean> {
    let lang = document.querySelector('html').getAttribute("lang");
    if (typeof lang === "undefined") {
      lang = "de"
    } else {
      if (this.config.debug) {
        console.log("Language detected: " + lang);
      }
    }
    lang = "_" + lang;
    const arr = window.location.href.split("/");
    const result = arr[0] + "//" + arr[2];
    const path = result + this.config.path;
    let file = path + "cookies" + lang + "."+this.config.extension;
    const fileExist = await this.doesFileExist(file);
    if (!fileExist) {
      lang = "_de";
      file = path + "cookies" + lang + "."+this.config.extension;
    }
    let response = await fetch(file);
    const json = await response.json();
    return new Promise(resolve => {
      this.splitJSON(json);
      resolve(true);
    });
  }

  /**
  * Function split JSON to variables
  * @param json
  * @private
  */
  private splitJSON(json: ContensJSON) {
    this.json_buttons = json.BUTTONS;
    this.json_cookies = json.COOKIES;
    this.json_details = json.DETAILS;
    this.json_dialog = json.DIALOG;
    this.json_links = json.LINKS;
    this.json_types = json.TYPES;
    this.json_types_aktiv = json.TYPES_AKTIV;
  }

  /**
  * Function check if file exist
  * @param urlToFile
  * @private
  */
  private async doesFileExist(urlToFile: string): Promise<boolean> {
    let response = await fetch(urlToFile);
    return response.status !== 404;
  }


  /**
  * Function run Replaces to activate the Third-Part Scripts
  * @param ele
  * @private
  */
  private runScript(ele: HTMLElement) {
    const tag = ele.tagName.toLowerCase();
    let isChange = false;
    switch (tag) {
      case "script":
        isChange = true;
        if (ele.getAttribute('type') === "text/javascript") {
          ele.setAttribute('type', 'text/plain');
        }
      break;
        case "link":
          isChange = true;
          if (typeof ele.getAttribute('href') !== "undefined") {
            ele.removeAttribute('href');
          }
        break;
          case "iframe":
            isChange = true;
            ele.dataset.src = ele.getAttribute('src');
            const link = ele.getAttribute('src');
            if (link.includes("youtube")) {
              let vidID = link.substr(0, link.indexOf("?"));
              if(vidID != ""){
                vidID = vidID.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
              }else{
                vidID = link.replace("https://www.youtube.com/embed/", "https://img.youtube.com/vi/");
              }
              vidID += "/maxresdefault.jpg";
              ele.style.backgroundImage = "url(" + vidID + ")";
              ele.style.backgroundSize = "contain";
            }
          ele.setAttribute('src', "");
            break;
            case "button":
              isChange = true;
              ele.dataset.allow = 'false';
              break;
    }
    return isChange;
  }




  render() {

    if (this.cookies.is_cookie_enabled()) {
      this.loadConfig();
      this.loadSettings();

      const cookie = this.cookies.get_cookie('COOKIE_CONFIRM_V3');
      if (cookie) {
        this.loadSettings(true);
        this.save(true);
        if (this.config.debug) {
          console.log('resetting builded');
        }
        return (
          <Host>
            <consent-settings></consent-settings>
          </Host>
          );
      } else {
        if (this.config.debug) {
          console.log('modal builded');
        }
        const url: string = window.location.href;
        if (!url.includes(this.json_links.PRIVACY.URL) || !url.includes(this.json_links.IMPRINT.URL)) {
          return (
            <Host>
              <consent-modal></consent-modal>
            </Host>
          );
        }
      }
    }
  }

}

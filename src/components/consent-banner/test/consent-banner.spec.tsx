import { newSpecPage } from '@stencil/core/testing';
import { ConsentBanner } from '../consent-banner';

describe('consent-banner', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentBanner],
      html: `<consent-banner></consent-banner>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-banner>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-banner>
    `);
  });
});

import { newE2EPage } from '@stencil/core/testing';

describe('consent-banner', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-banner></consent-banner>');

    const element = await page.find('consent-banner');
    expect(element).toHaveClass('hydrated');
  });
});

import { newE2EPage } from '@stencil/core/testing';

describe('consent-settings', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<consent-settings></consent-settings>');

    const element = await page.find('consent-settings');
    expect(element).toHaveClass('hydrated');
  });
});

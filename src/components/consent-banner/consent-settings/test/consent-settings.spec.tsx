import { newSpecPage } from '@stencil/core/testing';
import { ConsentSettings } from '../consent-settings';

describe('consent-settings', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [ConsentSettings],
      html: `<consent-settings></consent-settings>`,
    });
    expect(page.root).toEqualHtml(`
      <consent-settings>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </consent-settings>
    `);
  });
});
